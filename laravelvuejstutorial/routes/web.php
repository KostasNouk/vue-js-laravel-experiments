<?php

Route::get('/', function () {
    return view('welcome');
   });
Route::resource('items', 'ItemController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
