<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/{location?}', 'Controller@index');
Route::get('items/{slug}', 'Controller@itemsByLocation');
Route::post('locations/new','Controller@newLocation');
Route::post('items/new','Controller@newItem');
Route::post('items/edit','Controller@editItem');
